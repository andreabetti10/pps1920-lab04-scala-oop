package u04lab.code

import Optionals._
import Lists._
import Lists.List._
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions._

class PowerIteratorsTest {

  val factory = new PowerIteratorsFactoryImpl()

  @Test
  def testIncremental() {
    val pi = factory.incremental(5,_+2); // pi produce 5,7,9,11,13,...
    assertEquals(Option.of(5), pi.next());
    assertEquals(Option.of(7), pi.next());
    assertEquals(Option.of(9), pi.next());
    assertEquals(Option.of(11), pi.next());
    assertEquals(List.Cons(5, List.Cons(7, List.Cons(9, List.Cons(11,List.Nil())))), pi.allSoFar()); // elementi già prodotti
    for (i <- 0 until 10) {
      pi.next(); // procedo in avanti per un po'..
    }
    assertEquals(Option.of(33), pi.next()); // sono arrivato a 33
  }

  @Test
  def testRandom(): Unit = {
    val pi = factory.randomBooleans(4)
    val b1 = Option.getOrElse(pi.next(), Option.empty)
    val b2 = Option.getOrElse(pi.next(), Option.empty)
    val b3 = Option.getOrElse(pi.next(), Option.empty)
    val b4 = Option.getOrElse(pi.next(), Option.empty)

    assertTrue(pi.next == Option.empty)

    assertEquals(List.Cons(b1, List.Cons(b2, List.Cons(b3, List.Cons(b4,List.Nil())))), pi.allSoFar())
  }

  @Test
  def testFromList(): Unit = {
    val pi = factory.fromList(List.Cons(7, List.Cons(9, List.Cons(11,List.Nil()))))
    assertEquals(pi.next(), Option.of(7))
    assertEquals(pi.next(), Option.of(9))
    assertEquals(pi.allSoFar(),List.Cons(7, List.Cons(9, List.Nil())))

    assertEquals(pi.next(), Option.of(11))
    assertEquals(pi.allSoFar(), List.Cons(7, List.Cons(9, List.Cons(11,List.Nil()))))

    assertEquals(Option.empty, pi.next())
  }

  @Test
  def optionalTestReversedOnList(): Unit = {
    val pi = factory.fromList(List.Cons(7, List.Cons(9, List.Cons(11,List.Nil()))))
    assertEquals(pi.next(), Option.of(7))
    assertEquals(pi.next(), Option.of(9))
    val pi2 = pi.reversed()
    assertEquals(pi.next(), Option.of(11))

    assertEquals(Option.empty, pi.next)
    assertEquals(pi2.next(), Option.of(9))
    assertEquals(pi2.next(), Option.of(7))
    assertEquals(pi2.allSoFar(), List.Cons(9, List.Cons(7, List.Nil())))

    assertEquals(Option.empty, pi2.next())
  }

  @Test
  def optionalTestReversedOnIncremental(): Unit = {
    val pi = factory.incremental(0, x => x + 1)
    assertEquals(pi.next(), Option.of(0))
    assertEquals(pi.next(), Option.of(1))
    assertEquals(pi.next(), Option.of(2))
    assertEquals(pi.next(), Option.of(3))
    val pi2 = pi.reversed()
    assertEquals(pi2.next(), Option.of(3))
    assertEquals(pi2.next(), Option.of(2))
    val pi3 = pi2.reversed()
    assertEquals(pi3.next(), Option.of(2))
    assertEquals(pi3.next(), Option.of(3))
    assertEquals(Option.empty, pi3.next())
  }
}