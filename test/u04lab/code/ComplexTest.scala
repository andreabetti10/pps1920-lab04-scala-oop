package u04lab.code

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions._
import u04lab.code._

class ComplexTest {
  @Test def testToString(): Unit = {
    val c = Complex(10,20)
    assertEquals(c.toString(), "ComplexImpl(10.0,20.0)")
  }

  @Test def testEquals(): Unit = {
    val c = Complex(10,20)
    val c2 = Complex(10.0,20.0)
    assertEquals(c, c2)
  }

  @Test def testComplex(): Unit = {
    val a = Array(Complex(10,20), Complex(1,1), Complex(7,0))
    val c = a(0) + a(1) + a(2)
    val c2 = a(0) * a(1)
    assertEquals(c, ComplexImpl(18.0,21.0))
    assertEquals(c2, ComplexImpl(-10.0,30.0))
  }

}
