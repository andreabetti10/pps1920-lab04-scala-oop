package u04lab.code

import scala.util.Random
import Optionals._
import Lists._
import Streams.Stream
import Streams.Stream._

trait PowerIterator[A] {
  def next(): Option[A]
  def allSoFar(): List[A]
  def reversed(): PowerIterator[A]
}

trait PowerIteratorsFactory {

  def incremental(start: Int, successive: Int => Int): PowerIterator[Int]
  def fromList[A](list: List[A]): PowerIterator[A]
  def randomBooleans(size: Int): PowerIterator[Boolean]
}

class PowerIteratorImpl[A](var stream: Stream[A] = Stream.empty()) extends PowerIterator[A] {
  private var pastList = List.nil[A]
  override def next(): Option[A] = {
    stream match {
      case Cons(h, t) => {
        pastList = List.Cons(h.apply(), pastList)
        stream = t.apply()
        Option.of(h.apply())
      }
      case _ => Option.empty
    }
  }

  override def allSoFar(): List[A] = List.reverse(pastList)

  override def reversed(): PowerIterator[A] = new PowerIteratorImpl[A](List.toStream(pastList))
}

class PowerIteratorsFactoryImpl extends PowerIteratorsFactory {

  override def incremental(start: Int, successive: Int => Int): PowerIterator[Int] = new PowerIteratorImpl[Int](Stream.iterate(start)(successive))

  override def fromList[A](list: List[A]): PowerIterator[A] = new PowerIteratorImpl[A](List.toStream(list))

  override def randomBooleans(size: Int): PowerIterator[Boolean] = new PowerIteratorImpl[Boolean](Stream.take(Stream.generate(Random.nextBoolean()))(size))
}
